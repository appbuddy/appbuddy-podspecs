#
# Be sure to run `pod lib lint ThreeQSDN.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "ThreeQSDN"
  s.version          = "0.1.6"
  s.summary          = "Library to use 3QSDN in iOS-Apps"
  s.description      = <<-DESC
                       An optional longer description of ThreeQSDN

                       * Markdown format.
                       * Don't worry about the indent, we strip it!
                       DESC
  s.homepage         = "https://www.app-buddy.de/"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "Malte Fentroß" => "m.fentross@app-buddy.de" }
  s.source           = { :git => "git@bitbucket.org:3qsdn/tqsdn-pod.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'ThreeQSDN' => ['Pod/Assets/*.png']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
# s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'VideoCore', '~> 0.3.2'
  s.dependency 'JSONModel', '~> 1.1'
end
